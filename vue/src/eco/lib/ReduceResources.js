import Item from '../Item'
import { empty, nullish } from 'src/utils'
import * as items from 'src/data/items'

export default class ReduceResources {
  constructor (crafting) {
    this.crafting = crafting
  }

  has (object, key) {
    return Object.hasOwnProperty.call(object, key)
  }

  reduce (station = null) {
    if (station === null) {
      const results = {}

      for (const using in this.crafting) {
        if (this.has(this.crafting, using)) {
          results[using] = this.reduceResources(this.crafting[using])
        }
      }

      return results
    } else {
      return this.reduceResources(this.crafting[station])
    }
  }

  reduceRaw (resources) {
    const results = {}

    // Yield quantity is the index of the crafting time required
    let yields

    // Iterate through each crafting materials
    for (const resource in resources) {
      if (this.has(resources, resource)) {
        const quantity = resources[resource]

        if (/\d+/.test(resource)) {
          // Capture the quantity so we can multiply child resource costs
          yields = parseInt(resource)
        } else {
          // Flatten raw resource costs for each child
          const item = new Item(items[resource])
          const minimum = Math.ceil(quantity / yields)
          const current = nullish(results, item.name, 0)

          if (empty(item.crafting)) {
            // Empty crafting recipes means this is a raw resource
            results[item.name] = current + minimum
          } else {
            // Need to convert crafted items into their raw resources
            for (const raw in item.resources) {
              if (this.has(item.resources, raw)) {
                results[raw] = item.resources[raw] * Math.ceil(minimum / yields)
              }
            }
          }
        }
      }
    }

    return results
  }

  reduceResources (resources) {
    if (Array.isArray(resources)) {
      const results = []

      for (let i = 0; i < resources.length; i++) {
        results.push(this.reduceRaw(resources[i]))
      }

      return results
    } else {
      return this.reduceRaw(resources)
    }
  }
}
