import Item from '../Item'
import * as items from 'src/data/items'

export default class BuildTree {
  constructor (crafting) {
    this.crafting = crafting
  }

  build () {
    const results = {}

    for (const using in this.crafting) {
      if (this.has(this.crafting, using)) {
        const recipes = this.crafting[using]
        const station = items[using].name

        if (Array.isArray(recipes)) {
          results[station] = []

          for (let i = 0; i < recipes.length; i++) {
            results[station].push(this.buildRecipe(recipes[i]))
          }
        } else {
          results[station] = [this.buildRecipe(recipes)]
        }
      }
    }

    return results
  }

  buildRecipe (recipe) {
    const results = {
      items: [],
      time: 0,
      yields: 1
    }

    for (const name in recipe) {
      if (this.has(recipe, name)) {
        if (/\d+/.test(name)) {
          // Numerical index is recipe yeild and its value is the crafting time
          results.time = recipe[name]
          results.yields = parseInt(name)
        } else {
          // Everything else is an item we can construct
          results.items.push({
            instance: new Item(items[name]),
            quantity: recipe[name]
          })
        }
      }
    }

    return results
  }

  has (object, key) {
    return Object.hasOwnProperty.call(object, key)
  }
}
