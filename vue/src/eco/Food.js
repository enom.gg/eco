import Item from './Item'
import Nutrient from './Nutrient'

export default class Food extends Item {
  constructor (data) {
    super(data)

    this.calories = data.calories
    this.efficiency = Food.efficiency(data.nutrients, data.calories)
    this.nutrients = data.nutrients.map((data, i) => new Nutrient(data, i))
  }

  static efficiency (nutrients, calories) {
    return (nutrients.reduce((a, b) => a + b, 0) / (calories / 100)).toFixed(2)
  }
}
