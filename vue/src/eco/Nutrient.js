export default class Nutrient {
  constructor (value, index) {
    this.value = value
    this.color = Nutrient.colors()[index]
    this.label = Nutrient.labels()[index]
  }

  static colors () {
    return ['red', 'orange', 'yellow', 'green']
  }

  static labels () {
    return ['Carbs', 'Protein', 'Fat', 'Vitamins']
  }
}
