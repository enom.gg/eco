import BuildTree from './lib/BuildTree'
import ReduceResources from './lib/ReduceResources'
import { empty } from 'src/utils'

export default class Item {
  constructor (data) {
    this.class = data.class
    this.crafting = data.crafting
    this.name = data.name
    this.weight = data.weight

    // Raw materials have no crafting recipes
    this.raw = empty(data.crafting)

    // Only flatten resources and build tree on non-raw materials
    this.resources = this.raw ? {} : Item.resources(data.crafting)
    this.tree = this.raw ? {} : Item.tree(data.crafting)
  }

  // Reduce crafting station materials to their raw components
  static resources (crafting, station = null) {
    return new ReduceResources(crafting).reduce(station)
  }

  static tree (crafting) {
    return new BuildTree(crafting).build()
  }
}
