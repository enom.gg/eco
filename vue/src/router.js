// Package imports
import Vue from 'vue'
import VueRouter from 'vue-router'

// View imports
import ItemsAll from 'src/components/items/ItemsAll.vue'
import ItemsComponent from 'src/components/ItemsComponent.vue'
import ItemsOne from 'src/components/items/ItemsOne.vue'
import MealsComponent from 'src/components/MealsComponent.vue'

// Configure Vue to use router
Vue.use(VueRouter)

// Create and export router
export default new VueRouter({
  routes: [
    { name: 'meals', path: '/meals', component: MealsComponent },
    {
      path: '/items',
      component: ItemsComponent,
      children: [
        { name: 'items', path: '', component: ItemsAll },
        { name: 'items.one', path: ':item', component: ItemsOne }
      ]
    }
  ]
})
