export default function hydrate (source, data) {
  const has = Object.hasOwnProperty.call
  Object.keys(data).forEach((key) => has(source, key) && (source[key] = data[key]))
}
