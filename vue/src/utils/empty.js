export default function empty (v) {
  switch (typeof v) {
    case 'boolean':
      return v === false
    case 'number':
      return v === 0
    case 'object':
      if (v === null) return true
      if (Array.isArray(v)) return v.length === 0
      return Object.keys(v).length === 0
    case 'undefined':
      return true
    default:
      throw new Error('Unhandled empty (v) typeof: ' + (typeof v))
  }
}
