import isset from './isset'

export default function nullish (object, key, value) {
  return isset(() => object[key]) && object[key] !== null ? object[key] : value
}
