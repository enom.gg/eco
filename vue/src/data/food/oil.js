// Crafting station imports
import { Stove } from '../crafting-stations'

// Crafting material imports
import Tallow from './tallow'

export default {
  name: 'Oil',
  class: 'Oil',
  crafting: {
    [Stove.class]: {
      1: 5 * 60,
      [Tallow.class]: 18
    }
  }
}
