// Crafting station imports
import { Kitchen } from '../crafting-stations'

// Crafting material imports
import Huckleberries from './huckleberries'

export default {
  name: 'Huckleberry Extract',
  class: 'HuckleberryExtract',
  crafting: {
    [Kitchen.class]: {
      1: 5 * 60,
      [Huckleberries.class]: 50
    }
  }
}
