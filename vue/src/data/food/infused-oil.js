// Crafting station imports
import { Kitchen } from '../crafting-stations'

// Crafting material imports
import HuckleberryExtract from './huckleberry-extract'

export default {
  name: 'Infused Oil',
  class: 'InfusedOil',
  crafting: {
    [Kitchen.class]: {
      1: 5 * 60,
      [HuckleberryExtract.class]: 4
      // [Oil.class]: 4
    }
  }
}
