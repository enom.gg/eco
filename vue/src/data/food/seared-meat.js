// Crafting station imports
import { Stove } from '../crafting-stations'

// Crafting ingredient imports
import InfusedOil from './infused-oil'
import PrimeCut from './prime-cut'

export default {
  name: 'Seared Meat',
  class: 'SearedMeat',
  calories: 600,
  crafting: {
    [Stove.class]: {
      1: 8 * 60,
      [PrimeCut.class]: 5,
      [InfusedOil.class]: 4
    }
  },
  nutrients: [4.0, 19.0, 17.0, 7.0],
  weight: 0.5
}
