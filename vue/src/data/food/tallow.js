// Crafting station imports
import { Campfire } from '../crafting-stations'

// Crafting material imports
import RawMeat from './raw-meat'

export default {
  name: 'Tallow',
  class: 'Tallow',
  crafting: {
    [Campfire.class]: {
      2: 2 * 60,
      [RawMeat.class]: 6
    }
  }
}
