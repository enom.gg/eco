// Crafting station imports
import { ButcheryTable } from '../crafting-stations'

// Crafting ingredient imports
import RawMeat from './raw-meat'

export default {
  name: 'Prime Cut',
  class: 'PrimeCut',
  crafting: {
    [ButcheryTable.class]: {
      1: 2 * 60,
      [RawMeat.class]: 40
    }
  }
}
