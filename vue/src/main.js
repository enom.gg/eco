// Package imports
import Vue from 'vue'

// Application imports
import App from './App.vue'

// Router calls Vue.use(VueRouter)
import router from './router'

// Store calls Vue.use(Vuex)
import store from './store'

// Configuration
Vue.config.productionTip = false

// Boot
/* eslint-disable no-new */
new Vue({
  el: '#main',
  template: '<App/>',
  components: { App },
  router,
  store
})
