// Package imports
import Vue from 'vue'
import Vuex from 'vuex'

// Configure Vue to use VueX
Vue.use(Vuex)

// Create and export store
export default new Vuex.Store({
  state: {
    count: 0
  },
  mutations: {
    increment (state) {
      state.count++
    }
  }
})
