<?php

require 'vendor/autoload.php';

use Eco\Request;
use Eco\Decorate\Dom;

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title><?= Dom::$title ?></title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">
</head>
<body>
<div class="ui container">
    <?php

    if (Request::gets(Request::STORE)) {
        include 'views/store.php';
    } else if (Request::gets(Request::ITEMS)) {
        include 'views/items.php';
    } else if (Request::gets(Request::FOOD)) {
        include 'views/food.php';
    } else {
        include 'views/index.php';
    }

    ?>
</div>
</body>
</html>
