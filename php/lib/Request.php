<?php

namespace Eco;

class Request
{
    /**
     * $_GET property name to display food items and their nutrients.
     *
     * @var string
     */
    public const FOOD = 'food';

    /**
     * $_GET property name to display all items and their costs.
     *
     * @var string
     */
    public const ITEMS = 'items';

    /**
     * $_GET property name to display store inventory.
     *
     * @var string
     */
    public const STORE = 'store';

    /**
     * Returns the value of an existing $_GET property.
     *
     * @param string $name Property to query
     *
     * @return string
     */
    public static function get(string $name)
    {
        return self::gets($name) ? $_GET[$name] : null;
    }

    /**
     * Checks if the property exists in our $_GET request.
     *
     * @param string $name Property to query
     *
     * @return bool
     */
    public static function gets(string $name)
    {
        return isset($_GET[$name]);
    }
}
