<?php

namespace Eco\Decorate;

use Eco\Item\Food;

/**
 * Contains various colour mapped arrays.
 */
class Color
{
    /**
     * Returns the nutrient colour associated with a given label.
     *
     * @param string $label Label to colourise
     *
     * @return string
     */
    public static function uiNutrientColor(string $label)
    {
        return self::uiNutrientColors()[$label];
    }

    /**
     * Returns the list of colours associated with nutrients.
     *
     * @return array
     */
    public static function uiNutrientColors()
    {
        return array_combine(Food::LABELS, [
            'red',      // Carbs
            'orange',   // Protein
            'yellow',   // Fat
            'green',    // Vitamin
        ]);
    }
}
