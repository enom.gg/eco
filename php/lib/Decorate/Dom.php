<?php

namespace Eco\Decorate;

/**
 * Contains various shortcuts to generate HTML.
 */
class Dom
{
    /**
     * HTML header title.
     *
     * @var string
     */
    public static $title = 'Eco Trade Costs';
}
