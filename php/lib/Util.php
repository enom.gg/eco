<?php

namespace Eco;

class Util
{
    /**
     * Returns a sorted copy of the array based on the callback.
     *
     * @param array    $array    Array values to sort
     * @param callable $callback Callback used to sort
     *
     * @return array
     */
    public static function sort(array $array, callable $callback)
    {
        usort($array, $callback);

        return $array;
    }
}
