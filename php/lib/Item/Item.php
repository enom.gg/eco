<?php

namespace Eco\Item;

use Eco\Reduce\Item as Reducer;

/**
 * Base item class.
 */
class Item
{
    /**
     * Item crafting time.
     *
     * @var int
     */
    public $time = 0;

    /**
     * Item construction materials.
     *
     * @var array
     */
    public $crafting = [];

    /**
     * Item name.
     *
     * @var string
     */
    public $name = 'Item';

    /**
     * Item weight.
     *
     * @var float
     */
    public $weight = 0;

    /**
     * Item crafting yield count.
     *
     * @var int
     */
    public $yield = 1;

    /**
     * Returns the list of raw materials required to craft this item.
     *
     * @return array
     */
    public function resources()
    {
        return Reducer::item($this)->resources();
    }

    /**
     * Returns the list of all materials required to build this item.
     *
     * @return array
     */
    public function flatten(string $pad = '')
    {
        $flat = [];

        echo $pad.'flattening '.$this->name.'<br>';

        foreach ($this->crafting as $class => $quantity) {
            /** @var \Eco\Item\Item $material */
            $material = new $class();
            $P = $pad.$this->name;
            $N = $material->name;

            // Can't split items in fractions
            $minimum = ceil($quantity / $material->yield);
            $M = $minimum;

            // Material dependency distinguishes raw resources from others
            if (empty($material->crafting)) {
                echo $P.' - add '.$M.' '.$N.' as raw material<br>';

                // Raw resources can only be obtained in the open world
                $flat[$class] = ($flat[$class] ?? 0) + $minimum;
            } else {
                echo $P.' - add '.$M.' '.$N.'...<br>';
                // Flatten all craftable items into their raw resources
                foreach ($material->flatten($pad.' - ') as $child => $qty) {
                    $Q = $minimum * $qty / $material->yield;
                    echo $P.' ... add '.$Q.' of '.$child.'<br>';
                    $flat[$child] = ($flat[$child] ?? 0) + ceil($minimum * ($qty / $material->yield));
                }
            }

            // var_dump($flat);
        }

        return $flat;
    }
}
