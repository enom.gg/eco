<?php

namespace Eco\Item;

use Eco\Eco;

class Food extends Item
{
    /**
     * Labels for the nutrients array.
     *
     * @var string[]
     */
    public const LABELS = [
        'Carbs',
        'Protein',
        'Fat',
        'Vitamins',
    ];

    /**
     * Number of calories this food provides.
     *
     * @var int
     */
    public $calories;

    /**
     * List of nutrients this food provides.
     *
     * @var float[]
     */
    public $nutrients;

    /**
     * Returns the number of calories this food provides.
     *
     * @return int
     */
    public function calories()
    {
        return $this->calories;
    }

    /**
     * Calculates food efficiency.
     *
     * @return float
     */
    public function efficiency()
    {
        return round(array_sum($this->nutrients) / ($this->calories / 100), 2);
    }

    /**
     * Returns all of the food items.
     *
     * @param bool $construct Construct classes before returning
     *
     * @return array
     */
    public static function items(bool $construct = false)
    {
        $items = [
            Food\CerealGerm::class,
            Food\Corn::class,
            Food\CornFritters::class,
            Food\CrispyBacon::class,
            Food\Huckleberries::class,
            Food\HuckleberryExtract::class,
            Food\InfusedOil::class,
            Food\Oil::class,
            Food\PineappleFriendRice::class,
            Food\PrimeCut::class,
            Food\RawMeat::class,
            Food\SearedMeat::class,
            Food\SimmeredMeat::class,
            Food\StuffedTurkey::class,
            Food\Tallow::class,
            Food\SweetSalad::class,
            Food\VegetableSoup::class,
        ];

        return $construct ? array_map([Eco::class, 'make'], $items) : $items;
    }

    /**
     * Returns food items considered as meals.
     *
     * @param bool $construct Construct classes before returning
     *
     * @return array
     */
    public static function meals(bool $construct = false)
    {
        $items = [
            Food\CornFritters::class,
            Food\CrispyBacon::class,
            Food\PineappleFriendRice::class,
            Food\SearedMeat::class,
            Food\SimmeredMeat::class,
            Food\StuffedTurkey::class,
            Food\SweetSalad::class,
            Food\VegetableSoup::class,
        ];

        return $construct ? array_map([Eco::class, 'make'], $items) : $items;
    }

    /**
     * Returns the list of nutrients this food provides.
     *
     * @param bool $map Map nutrients to their labels
     *
     * @return float[]
     */
    public function nutrients(bool $map = false)
    {
        return $map ? array_combine(self::LABELS, $this->nutrients) : $this->nutrients;
    }
}
