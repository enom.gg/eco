<?php

namespace Eco\Item\Food;

use Eco\Item\CraftingStation\Stove;
use Eco\Item\Food;

class SearedMeat extends Food
{
    /**
     * Calories.
     *
     * @var int
     */
    public $calories = 600;

    /**
     * Crafting time.
     *
     * @var int
     */
    public $time = 8 * 60;

    /**
     * Materials.
     *
     * @var array
     */
    public $crafting = [
        Stove::class => [
            8 * 60,
            PrimeCut::class => 5,
            InfusedOil::class => 4,
        ],
    ];

    /**
     * Name.
     *
     * @var string
     */
    public $name = 'Seared Meat';

    /**
     * Nutrients.
     *
     * @var float[]
     */
    public $nutrients = [4.0, 19.0, 17.0, 7.0];

    /**
     * Weight.
     *
     * @var float
     */
    public $weight = 0.5;
}
