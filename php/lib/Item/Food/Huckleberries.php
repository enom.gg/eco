<?php

namespace Eco\Item\Food;

use Eco\Item\Food;

class Huckleberries extends Food
{
    /**
     * Calories.
     *
     * @var int
     */
    public $calories = 80;

    /**
     * Name.
     *
     * @var string
     */
    public $name = 'Huckleberries';

    /**
     * Nutrients.
     *
     * @var float[]
     */
    public $nutrients = [2.0, 0.0, 0.0, 6.0];

    /**
     * Weight.
     *
     * @var float
     */
    public $weight = 0.01;
}
