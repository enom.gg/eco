<?php

namespace Eco\Item\Food;

use Eco\Item\CraftingStation\Campfire;
use Eco\Item\Food;

class Tallow extends Food
{
    /**
     * Calories.
     *
     * @var int
     */
    public $calories = 200;

    /**
     * Crafting time.
     *
     * @var int
     */
    public $time = 2 * 60;

    /**
     * Materials.
     *
     * @var array
     */
    public $crafting = [
        Campfire::class => [
            2 => 2 * 60,
            RawMeat::class => 6,
        ],
    ];

    /**
     * Name.
     *
     * @var string
     */
    public $name = 'Tallow';

    /**
     * Nutrients.
     *
     * @var float[]
     */
    public $nutrients = [0.0, 0.0, 8.0, 0.0];

    /**
     * Weight.
     *
     * @var float
     */
    public $weight = 0.02;

    /**
     * Yield.
     *
     * @var int
     */
    public $yield = 2;
}
