<?php

namespace Eco\Item\Food;

use Eco\Item\Food;

class CrispyBacon extends Food
{
    /**
     * Calories.
     *
     * @var int
     */
    public $calories = 600;

    /**
     * Crafting time.
     *
     * @var int
     */
    public $time = 10 * 60;

    /**
     * Materials.
     *
     * @var array
     */
    public $crafting = [];

    /**
     * Name.
     *
     * @var string
     */
    public $name = 'Crispy Bacon';

    /**
     * Nutrients.
     *
     * @var float[]
     */
    public $nutrients = [0.0, 18.0, 26.0, 0.0];

    /**
     * Weight.
     *
     * @var float
     */
    public $weight = 0.2;
}
