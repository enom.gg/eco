<?php

namespace Eco\Item\Food;

use Eco\Item\Food;

class InfusedOil extends Food
{
    /**
     * Calories.
     *
     * @var int
     */
    public $calories = 120;

    /**
     * Crafting time.
     *
     * @var int
     */
    public $time = 5 * 60;

    /**
     * Materials.
     *
     * @var array
     */
    public $crafting = [
        Oil::class => 4,
        HuckleberryExtract::class => 4,
    ];

    /**
     * Name.
     *
     * @var string
     */
    public $name = 'Infused Oil';

    /**
     * Nutrients.
     *
     * @var float[]
     */
    public $nutrients = [0.0, 0.0, 12.0, 3.0];

    /**
     * Weight.
     *
     * @var float
     */
    public $weight = 0.1;
}
