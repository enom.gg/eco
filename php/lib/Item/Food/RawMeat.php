<?php

namespace Eco\Item\Food;

use Eco\Item\Food;

class RawMeat extends Food
{
    /**
     * Calories.
     *
     * @var int
     */
    public $calories = 250;

    /**
     * Crafting time.
     *
     * @var int
     */
    public $time = 60;

    /**
     * Materials.
     *
     * @var array
     */
    public $crafting = [];

    /**
     * Name.
     *
     * @var string
     */
    public $name = 'Raw Meat';

    /**
     * Nutrients.
     *
     * @var float[]
     */
    public $nutrients = [0.0, 7.0, 3.0, 0.0];

    /**
     * Weight.
     *
     * @var float
     */
    public $weight = 0.5;
}
