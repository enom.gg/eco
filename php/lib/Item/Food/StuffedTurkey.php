<?php

namespace Eco\Item\Food;

use Eco\Item\Food;

class StuffedTurkey extends Food
{
    /**
     * Calories.
     *
     * @var int
     */
    public $calories = 1500;

    /**
     * Crafting time.
     *
     * @var int
     */
    public $time = 30 * 60;

    /**
     * Materials.
     *
     * @var array
     */
    public $crafting = [];

    /**
     * Name.
     *
     * @var string
     */
    public $name = 'Stuffed Turkey';

    /**
     * Nutrients.
     *
     * @var float[]
     */
    public $nutrients = [9.0, 16.0, 12.0, 7.0];

    /**
     * Weight.
     *
     * @var float
     */
    public $weight = 1.0;
}
