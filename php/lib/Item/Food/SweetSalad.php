<?php

namespace Eco\Item\Food;

use Eco\Item\Food;

class SweetSalad extends Food
{
    /**
     * Calories.
     *
     * @var int
     */
    public $calories = 1100;

    /**
     * Crafting time.
     *
     * @var int
     */
    public $time = 15 * 60;

    /**
     * Materials.
     *
     * @var array
     */
    public $crafting = [];

    /**
     * Name.
     *
     * @var string
     */
    public $name = 'Sweet Salad';

    /**
     * Nutrients.
     *
     * @var float[]
     */
    public $nutrients = [18.0, 9.0, 7.0, 22.0];

    /**
     * Weight.
     *
     * @var float
     */
    public $weight = 0.4;
}
