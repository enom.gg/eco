<?php

namespace Eco\Item\Food;

use Eco\Item\Food;

class PineappleFriendRice extends Food
{
    /**
     * Calories.
     *
     * @var int
     */
    public $calories = 620;

    /**
     * Crafting time.
     *
     * @var int
     */
    public $time = 10 * 60;

    /**
     * Materials.
     *
     * @var array
     */
    public $crafting = [];

    /**
     * Name.
     *
     * @var string
     */
    public $name = 'Pineapple Friend Rice';

    /**
     * Nutrients.
     *
     * @var float[]
     */
    public $nutrients = [20.0, 12.0, 12.0, 9.0];

    /**
     * Weight.
     *
     * @var float
     */
    public $weight = 0.15;
}
