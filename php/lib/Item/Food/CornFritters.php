<?php

namespace Eco\Item\Food;

use Eco\Item\Food;

class CornFritters extends Food
{
    /**
     * Calories.
     *
     * @var int
     */
    public $calories = 500;

    /**
     * Crafting time.
     *
     * @var int
     */
    public $time = 5 * 60;

    /**
     * Materials.
     *
     * @var array
     */
    public $crafting = [];

    /**
     * Name.
     *
     * @var string
     */
    public $name = 'Corn Fritters';

    /**
     * Nutrients.
     *
     * @var float[]
     */
    public $nutrients = [15.0, 7.0, 17.0, 8.0];

    /**
     * Weight.
     *
     * @var float
     */
    public $weight = 0.1;
}
