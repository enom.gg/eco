<?php

namespace Eco\Item\Food;

use Eco\Item\Food;

class Corn extends Food
{
    /**
     * Calories.
     *
     * @var int
     */
    public $calories = 230;

    /**
     * Name.
     *
     * @var string
     */
    public $name = 'Corn';

    /**
     * Nutrients.
     *
     * @var float[]
     */
    public $nutrients = [5.0, 2.0, 0.0, 1.0];

    /**
     * Weight.
     *
     * @var float
     */
    public $weight = 0.01;
}
