<?php

namespace Eco\Item\Food;

use Eco\Item\Food;

class HuckleberryExtract extends Food
{
    /**
     * Calories.
     *
     * @var int
     */
    public $calories = 60;

    /**
     * Crafting time.
     *
     * @var int
     */
    public $time = 5 * 60;

    /**
     * Materials.
     *
     * @var array
     */
    public $crafting = [
        Huckleberries::class => 50,
    ];

    /**
     * Name.
     *
     * @var string
     */
    public $name = 'Huckleberry Extract';

    /**
     * Nutrients.
     *
     * @var float[]
     */
    public $nutrients = [0.0, 0.0, 0.0, 15.0];

    /**
     * Weight.
     *
     * @var float
     */
    public $weight = 0.2;
}
