<?php

namespace Eco\Item\Food;

use Eco\Item\Food;

class SimmeredMeat extends Food
{
    /**
     * Calories.
     *
     * @var int
     */
    public $calories = 900;

    /**
     * Crafting time.
     *
     * @var int
     */
    public $time = 10 * 60;

    /**
     * Materials.
     *
     * @var array
     */
    public $crafting = [];

    /**
     * Name.
     *
     * @var string
     */
    public $name = 'Simmered Meat';

    /**
     * Nutrients.
     *
     * @var float[]
     */
    public $nutrients = [6.0, 18.0, 13.0, 5.0];

    /**
     * Weight.
     *
     * @var float
     */
    public $weight = 0.8;
}
