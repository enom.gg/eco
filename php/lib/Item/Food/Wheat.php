<?php

namespace Eco\Item\Food;

use Eco\Item\Food;

class Wheat extends Food
{
    /**
     * Calories.
     *
     * @var int
     */
    public $calories = 130;

    /**
     * Name.
     *
     * @var string
     */
    public $name = 'Wheat';

    /**
     * Nutrients.
     *
     * @var float[]
     */
    public $nutrients = [6.0, 2.0, 0.0, 0.0];

    /**
     * Weight.
     *
     * @var float
     */
    public $weight = 0.01;
}
