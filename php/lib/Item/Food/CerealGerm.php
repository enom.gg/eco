<?php

namespace Eco\Item\Food;

use Eco\Item\Food;

class CerealGerm extends Food
{
    /**
     * Calories.
     *
     * @var int
     */
    public $calories = 20;

    /**
     * Crafting time.
     *
     * @var int
     */
    public $time = 5 * 60;

    /**
     * Materials.
     *
     * @var array
     */
    public $crafting = [
        Corn::class => 10,
        Wheat::class => 20,
    ];

    /**
     * Name.
     *
     * @var string
     */
    public $name = 'Cereal Germ';

    /**
     * Nutrients.
     *
     * @var float[]
     */
    public $nutrients = [5.0, 0.0, 7.0, 3.0];

    /**
     * Weight.
     *
     * @var float
     */
    public $weight = 0.01;
}
