<?php

namespace Eco\Item\Food;

use Eco\Item\Food;

class PrimeCut extends Food
{
    /**
     * Calories.
     *
     * @var int
     */
    public $calories = 600;

    /**
     * Crafting time.
     *
     * @var int
     */
    public $time = 2 * 60;

    /**
     * Materials.
     *
     * @var array
     */
    public $crafting = [
        RawMeat::class => 40,
    ];

    /**
     * Name.
     *
     * @var string
     */
    public $name = 'Prime Cut';

    /**
     * Nutrients.
     *
     * @var float[]
     */
    public $nutrients = [0.0, 9.0, 4.0, 0.0];

    /**
     * Weight.
     *
     * @var float
     */
    public $weight = 0.5;
}
