<?php

namespace Eco\Item\Food;

use Eco\Item\Food;

class VegetableSoup extends Food
{
    /**
     * Calories.
     *
     * @var int
     */
    public $calories = 1200;

    /**
     * Crafting time.
     *
     * @var int
     */
    public $time = 10 * 60;

    /**
     * Materials.
     *
     * @var array
     */
    public $crafting = [];

    /**
     * Name.
     *
     * @var string
     */
    public $name = 'Vegetable Soup';

    /**
     * Nutrients.
     *
     * @var float[]
     */
    public $nutrients = [12.0, 4.0, 7.0, 19.0];

    /**
     * Weight.
     *
     * @var float
     */
    public $weight = 0.9;
}
