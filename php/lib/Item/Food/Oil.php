<?php

namespace Eco\Item\Food;

use Eco\Item\Food;

class Oil extends Food
{
    /**
     * Calories.
     *
     * @var int
     */
    public $calories = 120;

    /**
     * Crafting time.
     *
     * @var int
     */
    public $time = 5 * 60; // 2 * 60

    /**
     * Materials.
     *
     * @var array
     */
    public $crafting = [
        Tallow::class => 18,
        // CerealGerm::class => 30,
    ];

    /**
     * Name.
     *
     * @var string
     */
    public $name = 'Oil';

    /**
     * Nutrients.
     *
     * @var float[]
     */
    public $nutrients = [0.0, 0.0, 12.0, 3.0];

    /**
     * Weight.
     *
     * @var float
     */
    public $weight = 0.1;
}
