<?php

namespace Eco\Item\CraftingStation;

use Eco\Item\CraftingStation;

class Campfire extends CraftingStation
{
    /**
     * Name.
     *
     * @var string
     */
    public $name = 'Campfire';
}
