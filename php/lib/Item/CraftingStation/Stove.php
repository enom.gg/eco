<?php

namespace Eco\Item\CraftingStation;

use Eco\Item\CraftingStation;

class Stove extends CraftingStation
{
    /**
     * Name.
     *
     * @var string
     */
    public $name = 'Stove';
}
