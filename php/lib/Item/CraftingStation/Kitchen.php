<?php

namespace Eco\Item\CraftingStation;

use Eco\Item\CraftingStation;

class Kitchen extends CraftingStation
{
    /**
     * Name.
     *
     * @var string
     */
    public $name = 'Kitchen';
}
