<?php

namespace Eco\Reduce;

class Item
{
    /**
     * Item to reduce.
     *
     * @var \Eco\Item\Item
     */
    public $item;

    /**
     * Item constructor.
     *
     * @param \Eco\Item\Item|string $item Item to reduce
     */
    public function __construct($item)
    {
        $this->item = is_string($item) ? new $item() : $item;
    }

    /**
     * Static constructor.
     *
     * @param \Eco\Item\Item|string $item Item to reduce
     *
     * @return \Eco\Reduce\Item
     */
    public static function item($item)
    {
        return new self($item);
    }

    /**
     * Returns the list of raw materials required to craft this item.
     *
     * @param \Eco\Item\CraftingStation|string|null $station Crafting station to use
     *
     * @return array
     */
    public function resources($station = null)
    {
        if (null === $station) {
            // Not specifying a station means display all
            $collect = [];

            // Iterate through each station
            foreach ($this->item->crafting as $using => $resources) {
                // Convert class names into readable names
                /** @var \Eco\Item\Item $item */
                $item = new $using();

                // Add to collection
                $collect[$item->name] = $this->reduceResources($resources);
            }

            return $collect;
        } else {
            // Reduce single station
            $index = is_string($station) ? $station : get_class($station);

            return $this->reduceResources($this->item->crafting[$index]);
        }
    }

    /**
     * @return int
     */
    protected function reduceLabour()
    {
        return 0;
    }

    /**
     * @return array
     */
    protected function reduceRaw(array $resources)
    {
        $collect = [];

        // Yield quantity is defined as an integer index in the resources
        $yield = null;

        foreach ($resources as $class => $quantity) {
            if (is_int($class)) {
                // Specifying no index for the duration means the yield is 1
                $yield = $class ?: 1;
            } else {
                // Everything else is a crafting material to reduce
                /** @var \Eco\Item\Item $resource */
                $resource = new $class();
                $name = $resource->name;
                $min = ceil($quantity / $yield);

                if (empty($resource->crafting)) {
                    echo $name.' is a raw resource<br>';
                    $collect[$name] = ($collect[$name] ?? 0) + $min;
                } else {
                    echo $name.' is a crafted item<br>';
                    $collect[$name] = ($collect[$name] ?? 0) + 1;
                }
            }
        }

        return $collect;
    }

    /**
     * @return array
     */
    protected function reduceResources(array $resources)
    {
        // Stations can have multiple recipes which is identified by an array of arrays
        if (is_array($resources[0])) {
            // Instead of displaying one recipe, display multiple
            $collect = [];

            // Iterate through each recipe the station has
            foreach ($resources as $recipe) {
                // Reduce each individual recipe
                $collect[] = $this->reduceRaw($recipe);
            }

            return $collect;
        } else {
            // Only has one recipe for the station
            return $this->reduceRaw($resources);
        }
    }
}
