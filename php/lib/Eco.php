<?php

namespace Eco;

use Eco\Item\Food;

/**
 * Base class for Eco trade calculation.
 */
class Eco
{
    /**
     * Returns the list of items.
     *
     * @param bool $construct Construct classes before returning
     *
     * @return array
     */
    public static function items(bool $construct = false)
    {
        $items = array_merge(...[
            Food::items(),
        ]);

        return $construct ? array_map([self::class, 'make'], $items) : $items;
    }

    /**
     * Returns the list of shops belonging to Enomonopoly.
     *
     * @return array
     */
    public static function shops()
    {
        return [];
    }

    /**
     * Creates a new class by it's fully qualified domain name.
     *
     * @param string $class     Class to new
     * @param array  $arguments Arguments to spread
     *
     * @return mixed
     */
    public static function make(string $class, array $arguments = [])
    {
        return new $class(...$arguments);
    }
}
