<?php

namespace Eco;

class Collection
{
    /**
     * Collection array.
     *
     * @var array
     */
    protected $collect = [];

    /**
     * Pushes an item into our collection.
     *
     * @param $item
     */
    public function add($item)
    {
        $this->collect[] = $item;
    }

    /**
     * Returns the current collection.
     *
     * @return array
     */
    public function all()
    {
        return $this->collect;
    }
}
