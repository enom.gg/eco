<?php

/**
 * View file for an ITEMS request.
 */

use Eco\Request;

?>

<h1>Items</h1>
<p><a href="?">Back</a></p>

<?php

if (empty($query = Request::get(Request::ITEMS))) {
    include 'partials/items-all.php';
} else if (class_exists($query)) {
    include 'partials/items-one.php';
} else {
    include 'partials/items-none.php';
}

?>
