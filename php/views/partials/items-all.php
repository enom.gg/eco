<?php

/**
 * Partial view file for displaying all available items.
 */

use Eco\Eco;
use Eco\Request;

?>

<ul>
    <?php foreach (Eco::items(true) as $item) { ?>
        <li>
            <a href="?<?= Request::ITEMS ?>=<?= get_class($item) ?>">
                <?= $item->name ?>
            </a>
        </li>
    <?php } // end foreach (Eco::items()) ?>
</ul>
