<?php

/**
 * Partial view file for displaying item not found errors.
 */

use Eco\Request;

?>

<p class="ui red inverted segment">Could not find <?= Request::get(Request::ITEMS) ?></p>

