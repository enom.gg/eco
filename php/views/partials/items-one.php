<?php

/**
 * Partial view file for displaying item information.
 *
 * @var \Eco\Item\Item $item
 */

use Eco\Request;
use Eco\Decorate\Dom;

$class = Request::get(Request::ITEMS);
$item = new $class;

Dom::$title = $item->name;

?>

<h2><?= $item->name ?></h2>
