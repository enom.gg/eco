<?php

/**
 * View file for a FOOD request.
 */

use Eco\Util;
use Eco\Item\Food;
use Eco\Decorate\Color;

// Sort food items by efficiency
$items = Util::sort(Food::meals(true), static function (Food $a, Food $b) {
    // Flip so highest efficiency is at the top
    return $b->efficiency() <=> $a->efficiency();
});

?>

<h1>Food</h1>
<a href="?">Back</a>

<?php foreach ($items as $item) { ?>
    <h2><?= $item->name ?></h2>

    <div class="ui mini statistics">
        <div class="statistic">
            <div class="value"><?= $item->calories ?></div>
            <div class="label">Calories:</div>
        </div>

        <div class="blue statistic">
            <div class="value"><?= $item->efficiency() ?></div>
            <div class="label">Efficiency:</div>
        </div>

        <?php foreach ($item->nutrients(true) as $label => $value) { ?>
            <div class="<?= Color::uiNutrientColor($label) ?> statistic">
                <div class="value"><?= $value ?></div>
                <div class="label"><?= $label ?></div>
            </div>
        <?php } // end foreach ($item->nutrients()) ?>
    </div>
<?php } // end foreach (Eco::items()) ?>
