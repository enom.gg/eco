<?php

/**
 * Default welcome view.
 */

use Eco\Request;

?>

<h1>Welcome</h1>
<p><a href="?<?= Request::STORE ?>">Store</a></p>
<p><a href="?<?= Request::ITEMS ?>">Items</a></p>
<p><a href="?<?= Request::FOOD ?>">Food</a></p>